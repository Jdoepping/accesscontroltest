﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyAccessControl.Exceptions;
using VideoOS.Platform.AccessControl.Elements;
using VideoOS.Platform.AccessControl.Plugin;
using VideoOS.Platform.AccessControl.TypeCategories;

namespace MyAccessControl.Classes
{

    public class ConfigurationValidator
    {
        public  void ValidateConfiguration(IEnumerable<ACElement> acElements)
        {
            var elements = acElements.ToList();

            ValidateServerElements(elements);
            MandatoryProperties(elements);
            //verify the ParentUnit on the ACUnits
            var units = elements.OfType<ACUnit>().ToList();

            NoUnitsAreOrphaned(units);
            CheckSelfReferences(units);

            var doors = units.Where(t => IsDoor(elements, t)).ToList();
            NoDoorIsAChildOfADoor(doors);
            AllAccessPointsAreChildrenOfADoor(elements, doors);
            // All units must have a reference to a type unit and the type unit must exists.
            DoesUnitsHaveAValidTypeReference(elements);
        }

        /// <summary>
        /// A unit where the parent reference points to it self
        /// </summary>
        /// <param name="elements"></param>
        public void CheckSelfReferences(IEnumerable<ACUnit> elements)
        {
            Func<ACUnit, bool> hasSelfReference = t => !string.IsNullOrWhiteSpace(t.ParentUnitId) && t.ParentUnitId == t.Id;
            var selfReferences = elements.Where(t => hasSelfReference(t)).ToList();
            if (selfReferences.Any())
            {
                throw new IsItsOwnParent("The unit " + selfReferences.First().Name + " points to it self as it's parent");
            }
        }

        public void AllAccessPointsAreChildrenOfADoor(List<ACElement> allAcElements, IEnumerable<ACUnit> doors)
        {
            var accessPoints = allAcElements.OfType<ACUnit>().Where(t => IsAccessPoint(allAcElements, t));
            var errorAccessPoints = accessPoints.Where(a => !doors.Select(d => d.Id).Contains(a.ParentUnitId)).ToList();
            if (errorAccessPoints.Any())
            {
                throw new AccessPointNotChildOfDoor("The parent of the access point ACUnit '" + errorAccessPoints.First().Name + "' must be a door ACUnit");
            }
        }

        public bool NoDoorIsAChildOfADoor(IEnumerable<ACUnit> doors)
        {
            //var doors = allACElements.OfType<ACUnit>().Where(t => IsDoor(allACElements, t));
            var errorDoors = doors.Where(t => doors.Select(p => p.Id).Contains(t.ParentUnitId)).ToList();
            if (!errorDoors.Any()) return true;

            var reportThis = errorDoors.First();
            var parentAcUnit = doors.First(t => t.Id == reportThis.ParentUnitId);
            throw new DoorCannotBeChildOfDoor("The door ACUnit '" + reportThis.Name + "' can not be a descendant of another door ('" + parentAcUnit.Name + "')");
        }

        /// <summary>
        /// A parent of a child is present
        /// </summary>
        /// <param name="elements"></param>
        public void NoUnitsAreOrphaned(IEnumerable<ACUnit> elements)
        {
            // All units with a parent reference must have its parent i.e. not be orphaned. 
            var parentReferences = elements.Where(t => !string.IsNullOrWhiteSpace(t.ParentUnitId)).Select(t => t.ParentUnitId);
            var parentIds = elements.Select(t => t.Id);

            var missing = parentReferences.Except(parentIds);
            if (missing.Any())
            {
                var errorElements = elements.Where(t => missing.Contains(t.ParentUnitId));
                // be lazy and report only a single 
                var element = errorElements.First();
                throw new UnitIsOrphaned("The unit " + element.Name + " points to a parent that is missing. It is orphaned");
            }
        }

        public void MandatoryProperties(List<ACElement> elements)
        {
            //verify that all ACElement has specified an ID and a Name, and that the ID is unique
            List<string> ids = new List<string>();
            foreach (var acElement in elements)
            {
                if (string.IsNullOrEmpty(acElement.Id))
                {
                    throw new ACConfigurationException("The ACElement with the name: '" + acElement.Name + "' does not have a Id.");
                }
                if (string.IsNullOrEmpty(acElement.Name))
                {
                    throw new ACConfigurationException("The ACElement with the id: '" + acElement.Id + "' does not have a Name.");
                }
                if (ids.Contains(acElement.Id))
                {
                    throw new ACConfigurationException("More than one ACElement has the id: '" + acElement.Id + "', " + acElement.Name);
                }
                ids.Add(acElement.Id);
            }
        }

        public void DoesUnitsHaveAValidTypeReference(List<ACElement> elements)
        {
            // All types must have a valid reference to an ACUnitType 
            var references = elements.OfType<ACInstance>().Select(t => t.InstanceTypeId);
            var r = elements.OfType<ACOperationableType>().Select(t => t.Id);
            var missing = references.Except(r).ToList();
            if (!missing.Any()) return;

            var errorElements = elements.OfType<ACInstance>().Where(t => missing.Contains(t.InstanceTypeId));
            // be lazy and report only a single 
            var element = errorElements.First();
            throw new MissingUnitType($"The unit \"{element.Name}\" is missing a reference to an ACUnitType element");
        }

        public void ValidateServerElements(IEnumerable<ACElement> allElements)
        {
            var elements = ServerElements(allElements).ToList();
            var validations = new Validation[]
            {
                new Validation {Criteria = "The configuration must contain one and only one ACServerType element.", Check = () => elements.OfType<ACServerType>().Count() == 1 },
                new Validation {Criteria = "The configuration must contain one and only one ACServer element.", Check = () => elements.OfType<ACServer>().Count() == 1},
                new Validation {Criteria = "The InstanceType of the ACServer element must match the Id of the ACServerType element.", Check = () => elements.OfType<ACServer>().First().InstanceTypeId == elements.OfType<ACServerType>().First().Id,
                    },
            };

            foreach (var validation in validations)
            {
                if (!validation.Check()) throw new ACConfigurationException(validation.Criteria);
            }
        }

        private class Validation
        {
            public Func<bool> Check { get; set; }
            public string Criteria { get; set; }
        }

        private IEnumerable<ACElement> ServerElements(IEnumerable<ACElement> elements)
        {
            return elements.Where(t => ((t as ACServerType) != null) || (t as ACServer) != null);
        }

        public bool IsDoor(List<ACElement> elements, ACUnit acUnit)
        {
            return BelongsToCategory(elements, acUnit, ACBuiltInUnitTypeCategories.Door);
        }

        public bool BelongsToCategory(List<ACElement> elements, ACUnit acUnit, string category)
        {
            var type = elements.OfType<ACUnitType>().First(t => acUnit.InstanceTypeId == t.Id);
            return type.Categories.Any(t => t == category);
        }

        public bool IsAccessPoint(List<ACElement> elements, ACUnit acUnit)
        {
            return BelongsToCategory(elements, acUnit, ACBuiltInUnitTypeCategories.AccessPoint);
        }

    }
}
