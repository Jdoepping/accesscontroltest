﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAccessControl.Exceptions
{
    public class AccessPointNotChildOfDoor : Exception
    {
        public AccessPointNotChildOfDoor(string msg) : base(msg)
        {
        }
    }
}
