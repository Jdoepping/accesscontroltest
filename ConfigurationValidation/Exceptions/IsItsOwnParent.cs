﻿using System;

namespace MyAccessControl.Exceptions
{
    /// <summary>
    /// Cannot be farther to itself
    /// </summary>
    public class IsItsOwnParent : Exception
    {
        public IsItsOwnParent(string msg) : base(msg)
        {
        }
    }
}
