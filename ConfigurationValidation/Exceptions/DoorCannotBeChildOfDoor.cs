﻿using System;

namespace MyAccessControl.Exceptions
{
    /// <summary>
    /// Thrown when the rule, that a door cannot be child of a door is violoated.
    /// </summary>
    public class DoorCannotBeChildOfDoor : Exception
    {
        public DoorCannotBeChildOfDoor(string msg) : base(msg)
        {
        }
    }
}
