﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAccessControl.Exceptions
{
    public class UnitIsOrphaned : Exception
    {
        public UnitIsOrphaned(string msg)
            : base(msg)
        {
        }
    }
}
