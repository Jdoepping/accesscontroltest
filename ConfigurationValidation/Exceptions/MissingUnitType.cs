﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAccessControl.Exceptions
{
    public class MissingUnitType : Exception
    {
        public MissingUnitType(string msg)
            : base(msg)
        {
        }
    }
}
