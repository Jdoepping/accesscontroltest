﻿namespace AccessControlTest.Classes
{
    /// <summary>
    /// This extended door adds the property "IsLocked" to the Door class.
    /// Giving it IsOpen and IsLocked properties  
    /// 
    /// This gives state combinations:
    /// Closed and locked
    /// Closed and not locked
    /// Open and not locked
    /// Open and locked
    ///
    /// To report the state using an ACState the StateIds would contain two elements.
    /// Potentially 4 icons/bitmaps would be needed.
    /// </summary>
    public class ExtendedDoor : Door
    {
        public override bool IsOpen { get; set; }

        public bool IsLocked { get; set; }
    }
}
