﻿namespace AccessControlTest.Classes
{
    public class Door
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Color { get; set; }

        public virtual bool IsOpen { get; set; }

    }
}
