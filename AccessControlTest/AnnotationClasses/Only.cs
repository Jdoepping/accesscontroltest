﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoOS.CustomDev.Annotations;
using VideoOS.Platform.AccessControl.Elements;
using VideoOS.Platform.AccessControl.Plugin;

namespace AccessControlTest.AnnotationClasses
{
    /// <summary>
    /// This converter only adds the unit and the unittype. No other configuration items are inserted, like commandstypes 
    /// </summary>
    public class OnlyUnitAndUnitType : IToAcClassConverter
    {
        private static int UnitIdCounter = 0;
        public virtual UnitType ConvertToUnitType(object obj, Func<string, string> translator = null) => new UnitType() { Id = obj.GetType().FullName };

        public virtual ACUnit ConvertToUnit(object obj, string parentTypeId, Func<string, string> translator = null)
        {
            var name = obj.GetType().FullName;
            // normally the id is read from the property the Id annotation 
            var id = name + (++UnitIdCounter);
            var u = ConvertToUnitType(obj, translator);
            return new ACUnit(id, name, "", new ACProperty[0], u.Id, parentTypeId);
        }
    }
}
