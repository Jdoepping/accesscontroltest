﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoOS.CustomDev.Annotations;
using VideoOS.CustomDev.Annotations.BuildInAnnotations;
using VideoOS.CustomDev.Annotations.BuiltIn;

namespace AccessControlTest.AnnotationClasses
{
    /// <summary>
    /// The annotations on the class level is used for two things:
    /// 1. The class will have a 1-1 relationship with an ACUnitType. The AcType class contains properties for the ACUnitType.
    /// 2. A class instance will become an instance of an ACunit. 
    /// 3. Command and Event annotations will define the commands and events associated with this class. it is possible to use already defined annotation classes
    /// like the LockCommand and UnlockCommand used here or generate a new annotation directly as it is done with the "[AcEventType(Name="Door Held Open")]" event annotation 
    /// definition.
    /// If any of the properties are set on the annotation LockCommand it becomes a new ACCommandType. If none of the parameters
    /// are changed it will be considered the same ACCommandType, so two classes both referring to LockCommand without changing it parameters will
    /// be referring to the same ACCommandType.     
    /// </summary>
    #region Annotations
    [AcType(Name = "Door", IconKey = AcBuiltInCategory.Icon.Door, Categories = new[] { AcBuiltInCategory.Type.Door, AcBuiltInCategory.Type.AccessPoint })]
    [LockCommand, UnlockCommand]
    [AcCommandType(Name = "Open Door")]
    [AcEventType(Name = "Door Held Open", Categories = new[] { "Alarm" })]
    [AcDoorClosedEvent, AcDoorOpenedEvent, AcDoorLockedEvent, AcDoorUnlockedEvent]
    [AcTypeProperty("DoorType", "123456"), AcTypeProperty("Model", "E34")]
    #endregion Annotations
    public class AnnotationDoor : ITreeItem
    {
        private List<ITreeItem> children;
        public ITreeItem Parent { get; set; }

        public void AddChild(ITreeItem item)
        {
            children = children ?? new List<ITreeItem>();
        }

        public IEnumerable<ITreeItem> Children
        {
            get { return children = children ?? new List<ITreeItem>(); }
        }

        [AcIdentity]
        public string Identity { get; set; }
        [AcName]
        public string Name { get; set; }

        [AcIcon]
        public string IconId { get; set; }

        /// <summary>
        /// State or Property?
        /// The IsLocked property is a State property. 
        /// This property can be said to be dynamic, because its value is expected to change over the lifetime of the object.
        /// Contrast it for example to the properties "DoorNumber" and "Color", which you would not expect to change during 
        /// the lifetime of the object.   
        /// The properties two possible values can be named: "Locked and Unlocked". Each value must be mapped to an ACStateType.
        /// This will happen automatically with the below annotations.
        /// </summary>
        [LockedState(AssociatedValue = true), UnlockedState(AssociatedValue = false)]
        public bool IsLocked { get; set; }

        /// <summary>
        /// This property is, like the "IsLocked" property, expected to change over the lifetime of the object. The associated ACStateTypes associated
        /// with two possible values of the property will be created automatically, using the values from the two AcStateType attributes. 
        /// </summary>
        [AcStateType(Name = "Open", IconKey = "OpenIcon", AssociatedValue = true)]
        [AcStateType(Name = "Closed", IconKey = "ClosedIcon", AssociatedValue = false)]
        public bool IsOpen { get; set; }

        /// <summary>
        /// The DoorNumber property is not expected to change during the lifetime of the property. 
        /// </summary>
        [AcProperty(Name = "Number")]
        public int DoorNumber { get; set; }

        [AcProperty]
        public string Color { get; set; }

        /// <summary>
        /// Below a definition of a local state type attribute. 
        /// </summary>
        [AcTypeAttribute(Name = "Locked", IconKey = "IconId")]
        private class LockedStateAttribute : AcStateTypeAttribute
        {
        }
    }
}
