﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using AccessControlTest.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyAccessControl.Classes;
using VideoOS.Platform.AccessControl.Elements;
using VideoOS.Platform.AccessControl.Plugin;
using VideoOS.Platform.AccessControl.TypeCategories;

namespace AccessControlTest
{
    [TestClass]
    public class CreateConfigurationTest
    {
        List<ACElement> elements;
        private ConfigurationValidator configValidator;


        [TestInitialize]
        public void Setup()
        {
            elements = new List<ACElement>();
            configValidator = new ConfigurationValidator();
        }

        /// <summary>
        /// Make the below test succeed
        /// </summary>
        [TestMethod]
        public void TestMakeConfiguration()
        {
            var door = new Door
            {
                Id = 1,
                Name = "Door One",
                Color = "Blue",
                IsOpen = true,
            };
            var serverUnit = new ACServer("server", "Server",null,null,"ServerTypeId");
            var serverType = new ACServerType("ServerTypeId","ServerType", null,null,null,null,null,null);
            elements.Add(serverUnit);
            elements.Add(serverType);
            var doorUnit = new ACUnit(door.Id.ToString(), door.Name, "iconKey", null, "doorType", null);
            var doorType = new ACUnitType("doorType", "DoorType",null,null,null,null,null,null);
            elements.Add(doorUnit);
            elements.Add(doorType);
            Check(elements);
        }

        [TestMethod]
        public void TestDoorMustBeOfCategoryDoor()
        {
            var door = new Door
            {
                Id = 1,
                Name = "Door One",
                Color = "Blue",
                IsOpen = true,
            };
            var serverUnit = new ACServer("server", "Server", null, null, "ServerTypeId");
            var serverType = new ACServerType("ServerTypeId", "ServerType", null, null, null, null, null, null);
            elements.Add(serverUnit);
            elements.Add(serverType);
            var doorUnit = new ACUnit(door.Id.ToString(), door.Name, "iconKey", null, "doorType", null);
            var doorType = new ACUnitType("doorType", "DoorType", null, null, new []{ACBuiltInUnitTypeCategories.Door}, null, null, null);
            elements.Add(doorUnit);
            elements.Add(doorType);
            Check(elements);
            // Build on what you did in "TestMakeConfiguration"
            // Make the test succeed
            Check(elements);
            // The configuration most contain at least one door
            Assert.IsTrue(ContainsDoor(elements), "Configuration does not contain a door");
        }

        [TestMethod]
        public void TestAddAdditionalDoor()
        {
            var door = new Door
            {
                Id = 1,
                Name = "Door One",
                Color = "Blue",
                IsOpen = true,
            };
            var serverUnit = new ACServer("server", "Server", null, null, "ServerTypeId");
            var serverType = new ACServerType("ServerTypeId", "ServerType", null, null, null, null, null, null);
            elements.Add(serverUnit);
            elements.Add(serverType);

            var doorUnit = new ACUnit(door.Id.ToString(), door.Name, "iconKey", null, "doorType", null);
            var doorUnit2 = new ACUnit("different", door.Name, "iconKey", null, "doorType", null);

            var doorType = new ACUnitType("doorType", "DoorType", null, null, new[] { ACBuiltInUnitTypeCategories.Door }, null, null, null);
            elements.Add(doorUnit);
            elements.Add(doorType);
            elements.Add(doorUnit2);
            // create a configuration that contains two doors.
            Check(elements);
            Assert.IsTrue(2<=DoorCount(elements), "Configuration contains less than two doors");
        }

        public void GoGetConfigFromAccessControlSystem()
        {
            
        }

        private List<ACElement> GetConfig()
        {
            var door = new Door
            {
                Id = 1,
                Name = "Door One",
                Color = "Blue",
                IsOpen = true,
            };
            var serverUnit = new ACServer("server", "Server", null, null, "ServerTypeId");
            var serverType = new ACServerType("ServerTypeId", "ServerType", null, null, null, null, null, null);
            elements.Add(serverUnit);
            elements.Add(serverType);

            var doorUnit = new ACUnit(door.Id.ToString(), door.Name, "iconKey", null, "doorType", null);
            var doorUnit2 = new ACUnit("different", door.Name, "iconKey", null, "doorType", null);

            var doorType = new ACUnitType("doorType", "DoorType", null, null, new[] { ACBuiltInUnitTypeCategories.Door }, new[] { "open", "closed" }, null, null);
            elements.Add(doorUnit);
            elements.Add(doorType);
            elements.Add(doorUnit2);
            return elements;
        }

        /// <summary>
        /// The Door class has an IsOpen property.
        /// This property must be part of the configuration
        /// Hint: Add state ()
        /// </summary>
        [TestMethod]
        public void TestDoorSupportsIsOpenState()
        {
            var elements = GetConfig();
            var stateOpen = new ACStateType("open", "Open",null,null,null);
            elements.Add(stateOpen);
            var stateClosed = new ACStateType("closed", "Closed", null, null, null);
            
            elements.Add(stateClosed);
            Check(elements);
            Assert.AreEqual(2, DoorStatesCount(elements), "State count is wrong");
        }
        /// <summary>
        /// Take a look at the "Color" property on Door class.
        /// Contrast it to "IsOpen" property. Which one changes during the lifetime of the door?
        /// Given the above add "Color" to the configuration.
        /// An ACUnit have a properties property, consider that 
        /// </summary>
        [TestMethod]
        public void TestDoorHasStaticPropertyColor()
        {
            // At least one door must have a property "Color" with value "Blue";
            Check(elements);
            Assert.IsTrue(MinimumOneDoorHasColorProperty(elements), "Config is missing a blue door!");
        }

        [TestMethod]
        public void TestDoorHasOpenCommand()
        {
            Check(elements);
            Assert.IsTrue(HasCommandNamedOpenDoor(elements), "Missing Open Door Command");
        }

        [TestMethod]
        public void TestDoorWithId1HasAnAccessPoint()
        {
            Check(elements);
            var doorId = "1";
            Assert.IsTrue(DoorHasAccessPointChild(doorId,elements),"Door is missing access point child");
        }

        #region Private methods
        private void Check(List<ACElement> config)
        {
            try
            {
                configValidator.ValidateConfiguration(elements);
                Assert.IsTrue(true);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        private bool ContainsDoor(List<ACElement> acElements)
        {
            var typeIds=acElements.OfType<ACUnitType>().Where(t => t.Categories.Contains(ACBuiltInUnitTypeCategories.Door)).Select(t=>t.Id);
            var acUnitTypesRefs = acElements.OfType<ACUnit>().Select(t => t.InstanceTypeId);
            return typeIds.Any(t => acUnitTypesRefs.Contains(t));
        }
        private int DoorCount(List<ACElement> acElements)
        {
            var doorType = acElements.OfType<ACUnitType>().First(t => t.Categories.Contains(ACBuiltInUnitTypeCategories.Door));
            return acElements.OfType<ACUnit>().Count(t => t.InstanceTypeId==doorType.Id);
        }

        private int DoorStatesCount(List<ACElement> acElements)
        {
            var doorType = acElements.OfType<ACUnitType>().First(t => t.Categories.Contains(ACBuiltInUnitTypeCategories.Door));
            return doorType.StateTypeIds.Count();
        }
        private bool MinimumOneDoorHasColorProperty(List<ACElement> acElements)
        {
            var doorTypeId = acElements.OfType<ACUnitType>().First(t => t.Categories.Contains(ACBuiltInUnitTypeCategories.Door)).Id;
            foreach (var door in acElements.OfType<ACUnit>().Where(t => t.InstanceTypeId == doorTypeId))
            {
                if (door.Properties.Any(t => t.Key == "Color" && t.Value == "Blue")) return true;
            }
            return false;
        }
        private bool HasCommandNamedOpenDoor(List<ACElement> acElements)
        {
            var doorType = acElements.OfType<ACUnitType>().First(t => t.Categories.Contains(ACBuiltInUnitTypeCategories.Door));
            var commandsOnDoor = acElements.Where(t => doorType.CommandTypeIds.Contains(t.Id)).Cast<ACCommandType>();
            return commandsOnDoor.Any(t => t.Name == "Open Door");
        }

        private bool DoorHasEvent(List<ACElement> acElements)
        {
            var doorType = acElements.OfType<ACUnitType>().First(t => t.Categories.Contains(ACBuiltInUnitTypeCategories.Door));
            var eventsOnDoor = acElements.Where(t => doorType.EventTypeIds.Contains(t.Id)).Cast<ACCommandType>();
            return eventsOnDoor.Any();
        }

        private bool DoorHasAccessPointChild(string doorId, List<ACElement> acElements)
        {
            var accessPointType = acElements.OfType<ACUnitType>().First(t => t.Categories.Contains(ACBuiltInUnitTypeCategories.AccessPoint));
            var accessPoints = acElements.OfType<ACUnit>().Where(t => t.InstanceTypeId == accessPointType.Id);
            return accessPoints.Any(t => t.ParentUnitId == doorId);
        }
        #endregion Private methods
    }
}
