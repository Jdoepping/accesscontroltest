﻿using System.Linq;
using AccessControlTest.AnnotationClasses;
using AccessControlTest.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VideoOS.CustomDev.Annotations;

namespace AccessControlTest
{
    [TestClass]
    public class AnnotationsTest
    {
        /// <summary>
        /// A single door is not a valid configuration. There must always be a server in a configuration.
        /// </summary>
        [TestMethod]
        [TestCategory("Annotation")]
        public void SingleDoorTest()
        {
            var converter = new OnlyUnitAndUnitType();
            var adapter = new Adapter(converter);

            var door = new AnnotationDoor();
            var configurationList = adapter.CreateConfiguration(new ITreeItem[] { door });
            //var unit = (UnitHelper)configurationList.First(t => t.GetType()==typeof(UnitHelper));
            //Assert.IsTrue(unit.);
            Assert.IsTrue(configurationList.Count() == 2);
        }
    }
}
